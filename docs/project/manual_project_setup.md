You can always manually create ScreenPlay content. ScreenPlay searches in every subfolder of you installed path for a project.json file. ScreenPlay should automatically detect changes in this path. If it's not working, you can always press F5 inside the Installed tab or restart ScreenPlay.

##### Example ScreenPlay Installed base path (672870 is the Steam App ID of ScreenPlay):

> C:\Program Files (x86)\Steam\steamapps\workshop\content\672870\

##### ScreenPlay Installed project inside the 2133699532 folder:

> C:\Program Files (x86)\Steam\steamapps\workshop\content\672870\2133699532\project.json

#### Typical content of a Wallpaper:

* project.json
* MyFancyWallpaper.webm
* audio.mp3
* preview.gif
* preview.jpg
* preview.webm
* previewThumbnail.jpg

A valid project contains a *project.json* inside a *subfolder* of your base path. The content of the project folder may vary, because Wallpaper and Widgets can have different types like videos, html website or QML 