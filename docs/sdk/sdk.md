The SDK is used for all communication between the Widgets, Wallpapers and external applications. This adds one major advatage: Crash save multi processing. If one Widgets/Wallpaper encounters an error it does not take down all other Widgets/Wallapers.



### Connection Process Exmaple 
1. User chooses an Widget to start -> **ScreenPlay::createWidget(...)** gets called
2. A new ScreenPlayWidget object gets instantiated and saved in the **m_screenPlayWidgetList** vector
3. The ScreenPlayWidget now determents in the constructor the type of project  (qml or custom executable)
4. If the widgets is a pure qml widget the we start our ScreenPlayWidget App or else we start their custom executable
5. We use process arguments to deliver some start settings (for example default plays peed and volume) and more importantly assign an **appID** to every active Widget/Wallpaper.


### Using the appID to communicate between ScreenPlay and the Widgets/Wallpaper
When the Wallpaper/Widget started it uses the **appID** from the starting arguments to register itself via **QLocalSocket**. When the appID is valid ScreenPlay keeps the connection open otherwise it closes the connection. A closing connection causes the SDK to call **QApplication::quit()** and so to exit application.

When a widget/wallpaper uses custom [settings](/project/project/) to enable a wide range of customization. The can be updated in real time via the Screen Tab in ScreenPlay. Theses settings are simple key/value pairs. For example when the user changes his wallpaper volume the **SDKConnector::setWallpaperValue** method sends the Json Object: 

```json
{
    "volume" : 1
}
```

The SDK then emits the **void incommingMessage(QString key, QString value)** method to listen to.