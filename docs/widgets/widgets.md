You can create Widgets via the build in wizard:


![RSS example](wizard.png "RSS  example")


Widgets are custom QML files which gets executed via the ScreenPlayWidget app. You can learn more about how to develop your widget in qml (even without prior programming skill!) via the fantastic qml book.

- [Getting started with qml](http://qmlbook.github.io/)


## Simple interactive button example

[Try this code in your browser!](https://tinyurl.com/y933aksb)

```qml
import QtQuick 2.14
import QtQuick.Controls 2.14


Item {
    id:root
    anchors.fill:parent

    Rectangle {
        color: "#333333"
        width:300
        height:300
        opacity: 0.5
        anchors.centerIn: parent
        Button {
            id:txtHello
            text: qsTr("Hello")
            anchors.centerIn: parent
            onClicked:{
                txtHello.text = "Hello Clicked"
            }
        }
    }
}

```


## Sysinfo to display hardware stats
Just import the SysInfo project in the beginning of your QML file:

```qml
import ScreenPlay.Sysinfo 1.0
```
Example:
```qml
import QtQuick 2.14
import QtQuick.Controls 2.14
import ScreenPlay.Sysinfo 1.0

Item {
    id:root
    anchors.fill: parent

    Rectangle {
        color: "white"
        text: "My CPU Usage: " + SysInfo.cpu.usage 
        anchors.centerIn: parent
    }
}
```

```qml
 // CPU
 - SysInfo.cpu.usage 
 - SysInfo.cpu.tickRate //Update every second

 // RAM
 - SysInfo.ram.usage

 - SysInfo.ram.usedPhysicalMemory
 - SysInfo.ram.totalPhysicalMemory

 - SysInfo.ram.usedVirtualMemory
 - SysInfo.ram.totalVirtualMemory

 - SysInfo.ram.usedPagingMemory
 - SysInfo.ram.totalPagingMemory

  // Storage (list model)
  // These variables are available for every listed storage device
  - name
  - displayName
  - isReadOnly
  - isReady
  - isRoot
  - isValid
  - bytesAvailable
  - bytesTotal
  - bytesFree
  - fileSystemType
```

##### Examples:
 - [Example CPU](example_CPU.md) 

![CPU Usage example](cpu_widget.png "CPU Usage example")

 - [Example Storage](example_Storage.md)

![Storage info example](storage_widget.png "Storage info example")

 - [Example RSS](example_RSS.md)
 
![RSS example](rss_widget.png "RSS  example")

### Sysinfo platform support

| Feature                	| Windows 	| Linux 	| MacOS 	|
|------------------------	|---------	|-------	|-------	|
| __CPU__              	    | ✔       	| ❌       | ❌     	|
| __RAM__                   | ✔       	| ❌       | ❌     	|
| __Storage__               | ✔        	|  ✔      |   ✔   	|
| __Network__               | ❌        	|  ❌     |  ❌     	|
| __GPU__       	        | ❌        	| ❌    	 |     ❌  	|


### Custom C++ Logic
You can add custom QML plugins in the root folder of your widget. These then get automatically loaded at startup.

 - [Creating C++ Plugins for QML](http://doc.qt.io/qt-5/qtqml-modules-cppplugins.html)
