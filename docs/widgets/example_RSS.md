
![RSS example](rss_widget.png "RSS  example")

```qml
import QtQuick 2.12
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.2
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.3
import QtQuick.XmlListModel 2.0

GridView {
    id: changelogFlickableWrapper
    flickableDirection: Flickable.VerticalFlick
    maximumFlickVelocity: 5000
    flickDeceleration: 5000
    cellHeight: 100
    cellWidth: parent.width

    model: feedModel

    Timer {
        interval: 20000
        running: true
        repeat: false
        triggeredOnStart: true
        onTriggered: {
            feedModel.source = "https://isocpp.org/blog/rss/category/news"
        }
    }

    XmlListModel {
        id: feedModel

        query: "/rss/channel/item"
        XmlRole {
            name: "title"
            query: "title/string()"
        }
        XmlRole {
            name: "link"
            query: "link/string()"
        }
        XmlRole {
            name: "pubDate"
            query: "pubDate/string()"
        }
    }

    header: Item {
        height: 60
        width: parent.width

        Text {
            id: name
            text: qsTr("ISO CPP Recent Highlights")
            wrapMode: Text.WordWrap
            color: "#333333"

            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pointSize: 12
            font.family: "Roboto"

            anchors {
                top: parent.top
                topMargin: 30
                horizontalCenter: parent.horizontalCenter
            }
        }
    }

    delegate: Item {
        id: root
        height: 100
        width: parent.width

        Rectangle {
            color: "#55ffffff"
            anchors.fill: parent
            anchors.margins: 5
            radius: 4

            ColumnLayout {
                  anchors.fill: parent
                   anchors.margins: 10
                Item {
                    Layout.fillWidth: true
                    Layout.preferredHeight: parent.height *.8
                    Text {
                        id: txtTitle
                        text: title
                        color: "#333333"
                        anchors.fill: parent
                        font.family: "Roboto"
                        font.weight: Font.Normal
                        font.pointSize: 10
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    }
                }

                Text {
                    id: txtPubDate
                     Layout.fillWidth: true
                     Layout.preferredHeight: parent.height *.2
                    text: pubDate.replace("+0000", "")
                    color: "#666666"

                    font.family: "Roboto"

                    font.weight: Font.Normal
                    font.pointSize: 10
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }
            }

            MouseArea {
                z:99
                anchors.fill: parent
                onClicked: Qt.openUrlExternally(link)
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor
            }
        }
    }

    ScrollBar.vertical: ScrollBar {
        snapMode: ScrollBar.SnapOnRelease
    }
}
```
