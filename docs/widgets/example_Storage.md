
![Storage info example](storage_widget.png "Storage info example")

```qml
import QtQuick 2.14
import QtQuick.Controls 2.14
import ScreenPlay.Sysinfo 1.0

Item {
    id: root
    width:100
    height: 100
    Text {
        id: type
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: 10
        }

        text: qsTr("Storage USAGE")
        font.pointSize: 22
        color: "white"
        font.family: "Segoe UI"
    }

    ListView {
        id: listView
        anchors {
            top:type.bottom
            right:parent.right
            bottom: parent.bottom
            left:parent.left
            margins: 10
        }

        model: SysInfo.storage
        delegate: Item {
            x: 5
            width: 80
            height: 40
            Row {
                id: row1
                spacing: 10
                Text {
                    text: name
                    font.bold: true
                    anchors.verticalCenter: parent.verticalCenter
                }
                ProgressBar {
                    from: 0
                    to:bytesTotal
                    value: bytesAvailable
                }
            }
        }
    }


}



```
