# Welcome to the official ScreenPlay Documentation
Here you can learn more about the different aspects of ScreenPlay:

- [Contribute - Code, Design & Translations](contribute/contribute)
- [ScreenPlay QML & C++ Class Documentation](https://kelteseth.gitlab.io/ScreenPlayDeveloperDocs/)

### Guides
- [Learn the basics of QML for Wallpapers and Widgets in 5 minutes](https://screen-play.app/blog/guide_learn_the_basics_of_qml/)
- [How To Create Video Wallpaper From Your Favorite Game](https://screen-play.app/blog/guide_create_seamless_video_wallpaper/)

### ScreenPlay Content
- [Learn more about projects](project/project)
- [Widget creation and custom C++](widgets/widgets)
- [Video, QML and HTML Wallpaper](wallpaper/wallpaper)
- [Develop own Apps via the ScreenPlaySDK](sdk/sdk)
