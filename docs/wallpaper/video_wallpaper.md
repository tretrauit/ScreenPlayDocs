
### Setup FFMPEG
![FFMPEG Download](video_wallpaper_create.png "FFMPEG Download")

### Convert a exsisting video to a wallpaper

![ Choose Codec ](video_wallpaper_create_codec.png "Choose Codec")

### Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper!

![ Convert the Video](video_wallpaper_create_convert.png "")

### This does multiple steps:
1. Create a 5 second preview
2. Create a preview Thumbnail
3. Extract the audio
4. Convert the video (two pass encoding for better quality)
