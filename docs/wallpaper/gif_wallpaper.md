### How to create a gif wallpaper

1. Go to `Create -> Gif Wallpaper`
![create_gif_1](create_gif_1.png "create_gif_1")

1. Go back to the Installed list and refresh via F5 or "pull to refresh"
