### How can I help with the translations/incorrect spelling?

1. Open the translations file (.ts) in the translation ScreenPlay git source folder
![ScreenPlay_translations](ScreenPlay_translations.png "ScreenPlay_translations")
1. Change them and save.
1. Create a gitlab merge request or ask in Forum/Discord/IRC, we will integrate the new langague for you.

### How do I add new languages?
1. Copy a pre existing langauge file and change the name
2. Search for your langauge code (french in this example)
   * http://www.lingoes.net/en/translator/langcode.htm
3. Open your langauge ts ScreenPlay_fr.ts and change the language="XXX" to your liking
4. Translate text by text. [DeepL](https://www.deepl.com/translator) can help with some languages!
5. Create a gitlab merge request or ask in Forum/Discord/IRC, we will integrate the new langague for you.